#!/usr/bin/env python
# coding: utf-8

# # Water Quality Prediction Model using LuciferML
# #By- Aarush Kumar
# #Dated: June 25,2021

# In[1]:


get_ipython().system('pip install lucifer-ml')


# In[2]:


import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
plt.style.use('fivethirtyeight')
plt.style.use('dark_background')
from matplotlib.colors import ListedColormap
from scipy.stats import norm, boxcox
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from collections import Counter
from scipy import stats
from tqdm import tqdm_notebook
## Importing LuciferML
from luciferml.supervised import classification as cls 
from luciferml.preprocessing import Preprocess as prep


# In[3]:


dataset=pd.read_csv(r'/home/aarush100616/Downloads/Projects/Water Quality Prediction/water_potability.csv')


# In[4]:


dataset


# ## EDA

# In[5]:


dataset.head()


# In[6]:


dataset.shape


# In[7]:


dataset.size


# In[8]:


dataset.info()


# In[9]:


dataset.isnull().sum()


# In[10]:


dataset.describe()


# In[11]:


dataset.describe().T.style.bar(
    subset=['mean'],
    color='#606ff2').background_gradient(
    subset=['std'], cmap='PuBu').background_gradient(subset=['50%'], cmap='PuBu')


# In[12]:


dataset.dropna(inplace =True)


# ## Countplot for Potability

# In[13]:


plt.figure(figsize=(12, 6))
sns.countplot(x="Potability", data=dataset, palette='husl');


# In[14]:


cols = ['ph', 'Hardness', 'Solids', 'Chloramines', 'Sulfate', 'Conductivity',
       'Organic_carbon', 'Trihalomethanes', 'Turbidity']


# In[15]:


def boxPlotter(dataset, columnName):
    """
    Plots boxplots for column given as parameter.
    """
    sns.catplot(x="Potability", y=columnName, data=dataset, kind="box");
for column in tqdm_notebook(cols, desc = "Your Charts are being ready"):
    boxPlotter(dataset, column)


# ## Pie Chart

# In[16]:


def pieChartPlotter(dataset, columnName):
    """
    Creates pie chart of the column given as parameter in the dataset
    """
    values = dataset[columnName].value_counts()
    labels = dataset[columnName].unique()
    pie, ax = plt.subplots(figsize=[10, 6])
    patches, texts, autotexts = ax.pie(values, labels=labels, autopct='%1.2f%%', shadow=True, pctdistance=.5,explode=[0.06]*dataset[columnName].unique()
                                      )
    plt.legend(patches, labels, loc="best")
    plt.title(columnName, color='white', fontsize=14)
    plt.setp(texts, color='white', fontsize=20)
    plt.setp(autotexts, size=10, color='black')
    autotexts[1].set_color('black')
    plt.axis('equal')
    plt.tight_layout()
    plt.show()


# In[17]:


pieChartPlotter(dataset, 'Potability') 


# ## Correlation Plot

# In[19]:


plt.figure(figsize=(20, 17))
matrix = np.triu(dataset.corr())
sns.heatmap(dataset.corr(), annot=True,linewidth=.8, mask=matrix, cmap="rocket");


# ## Distribution Plot

# In[20]:


def distributionPlot(dataset):
    fig = plt.figure(figsize=(20, 20))
    for i in tqdm_notebook(range(0, len(dataset.columns)), desc = 'Your plots are being ready'):
        fig.add_subplot(np.ceil(len(dataset.columns)/3), 3, i+1)
        sns.distplot(
            dataset.iloc[:, i], color="lightcoral", rug=True)
        fig.tight_layout(pad=3.0)


# In[21]:


plot_data = dataset.drop(['Potability'], axis =1)


# In[22]:


distributionPlot(plot_data)


# ## PairPlots

# In[23]:


sns.pairplot(dataset, hue="Potability", palette="husl");


# ## Data Preprocessing

# ### Skewness Correction

# In[24]:


dataset = prep.skewcorrect(dataset,except_columns=['Potability'])


# ### Featuring & Labelling

# In[25]:


features = dataset.iloc[:, :-1]
labels = dataset.iloc[:, -1]


# In[26]:


features,labels


# In[27]:


features.shape, labels.shape


# ## Model Training

# In[28]:


accuracy_scores = {}


# ### 1)- Logistic Regression

# In[29]:


params = {'C': 0.00026366508987303583, 'penalty': 'l2', 'solver': 'newton-cg'}
classifier_name,accuracy = cls.Classification(predictor = 'lr', params=params).predict(features, labels)
accuracy_scores[classifier_name] = accuracy


# ### 2)- SVM

# In[30]:


params = {'C': .75, 'gamma': 0.2,
          'kernel': 'linear', 'random_state': 42}
classifier_name,accuracy = cls.Classification(predictor = 'svm', 
                                              params= params, 
                                             ).predict(features, labels)
accuracy_scores[classifier_name] = accuracy


# ### 3)- K-Nearest Neighbour

# In[31]:


params = {'algorithm': 'kd_tree', 'n_jobs': 1, 'n_neighbors': 1, 'weights': 'uniform'}
classifier_name,accuracy = cls.Classification(predictor = 'knn', 
                                              params= params, 
                                             ).predict(features, labels)
accuracy_scores[classifier_name] = accuracy


# ### 4)- Decision Tree

# In[32]:


params = {'criterion': 'entropy', 'max_depth': 30, 'max_features': 'sqrt', 'splitter': 'best', 'random_state': 42}
classifier_name,accuracy = cls.Classification(predictor = 'dt', 
                                              params= params, 
                                             ).predict(features, labels)
accuracy_scores[classifier_name] = accuracy


# ### 5)- Naive Bayes

# In[33]:


classifier_name,accuracy = cls.Classification(predictor = 'nb', 
                                             ).predict(features, labels)
accuracy_scores[classifier_name] = accuracy


# ### 6)- Random Forest Classifier

# In[34]:


params = {'criterion': 'gini', 'max_features': 'auto', 'n_estimators': 150,'random_state':0}
classifier_name,accuracy = cls.Classification(predictor = 'rfc', 
                                              params= params, 
                                             ).predict(features, labels)
accuracy_scores[classifier_name] = accuracy


# ### 7)- XGBoost Classifier

# In[35]:


params = {'learning_rate':0.02, 'n_estimators':600, 'objective':'binary:logistic', 'eval_metric':'logloss', 'nthread':-1}
classifier_name,accuracy = cls.Classification(predictor = 'xgb', 
                                              params= params, 
                                             ).predict(features, labels)
accuracy_scores[classifier_name] = accuracy


# ## Chart for Model Accuracy

# In[37]:


plt.figure(figsize=(12, 6))
model_accuracies = list(accuracy_scores.values())
model_names = list(accuracy_scores.keys())
sns.barplot(x=model_accuracies, y=model_names, palette='mako');

